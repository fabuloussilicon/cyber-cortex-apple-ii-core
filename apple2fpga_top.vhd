-------------------------------------------------------------------------------
--
-- DE2 top-level module for the Apple ][
--
-- Stephen A. Edwards, Columbia University, sedwards@cs.columbia.edu
--
-- From an original by Terasic Technology, Inc.
-- (APPLE2_TOP.v, part of the DE2 system board CD supplied by Altera)
--
-------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity APPLE2_TOP is

  port (
    -- Clocks
    
    CLK_28M	: in std_logic;							-- 28 MHz
    CLK_14M   : in std_logic;                     -- 14 MHz

	 KBD_CLK   : out std_logic;
	 KBD_RST   : out std_logic;
	 KBD_READ  : out std_logic;
	 KBD_DATA  : in unsigned(7 downto 0);
	 
	 
	 

    -- Buttons and switches
    
    KEY : in std_logic_vector(3 downto 0);         -- Push buttons

    -- SRAM
    
    SRAM_DQ : inout unsigned(15 downto 0);         -- Data bus 16 Bits
    SRAM_ADDR : out unsigned(17 downto 0);         -- Address bus 18 Bits
    SRAM_UB_N :out std_logic;                                     -- High-byte Data Mask 
    SRAM_LB_N :out std_logic;                                     -- Low-byte Data Mask 
    SRAM_WE_N :out std_logic;                                     -- Write Enable

    SRAM_CE: out std_logic;                                     -- Chip Enable
	 SRAM_CS0: out std_logic;
	 SRAM_CS1: out std_logic;
	 SRAM_ADSC: out std_logic;
	 SRAM_ADSP: out std_logic;
	 SRAM_ADV: out std_logic;
	 SRAM_CLK: out std_logic;
	 SRAM_GW : out std_logic;                        --Write Enable;
    SRAM_OE_N : out std_logic;                     -- Output Enable

	 DIVIDER : out std_logic;	
    I_RESET : in std_logic;

	 SPEAKER_OUT_L : out std_logic;
	 SPEAKER_OUT_R : out std_logic;

    -- SD card interface
    
    MISO_IN : in std_logic;      -- SD Card Data      SD pin 7 "DAT 0/DataOut"
    CS_N_OUT : out std_logic;    -- SD Card Data 3    SD pin 1 "DAT 3/nCS"
    MOSI_OUT : out std_logic;     -- SD Card Command   SD pin 2 "CMD/DataIn"
    SCLK_OUT : out std_logic;     -- SD Card Clock     SD pin 5 "CLK"

    -- PS/2 port

  --  PS2_DAT,                    -- Data
  --  PS2_CLK : in std_logic;     -- Clock

    -- VGA output
    
    VGA_CLK,                                            -- Clock
    VGA_HS,                                             -- H_SYNC
    VGA_VS,                                             -- V_SYNC
    VGA_BLANK,                                          -- BLANK
    VGA_SYNC : out std_logic;                           -- SYNC
    VGA_R,                                              -- Red[9:0]
    VGA_G,                                              -- Green[9:0]
    VGA_B : out unsigned(9 downto 0)                    -- Blue[9:0]

    );
  
end APPLE2_TOP;

architecture datapath of APPLE2_TOP is
  
  signal CLK_2M, PRE_PHASE_ZERO : std_logic;
  signal IO_SELECT, DEVICE_SELECT : std_logic_vector(7 downto 0);
  signal ADDR : unsigned(15 downto 0);
  signal D, PD : unsigned(7 downto 0);

  signal ram_we : std_logic;
  signal VIDEO, HBL, VBL, LD194 : std_logic;
  signal COLOR_LINE : std_logic;
  signal COLOR_LINE_CONTROL : std_logic;
  signal GAMEPORT : std_logic_vector(7 downto 0);
  signal cpu_pc : unsigned(15 downto 0);

  signal K : unsigned(7 downto 0);
  signal read_key : std_logic;

  signal flash_clk : unsigned(22 downto 0) := (others => '0');
  signal power_on_reset : std_logic := '1';
  signal reset : std_logic;

  signal speaker : std_logic;

  signal track : unsigned(5 downto 0);
  signal image : unsigned(9 downto 0);
  signal trackmsb : unsigned(3 downto 0);
  signal D1_ACTIVE, D2_ACTIVE : std_logic;
  signal track_addr : unsigned(13 downto 0);
  signal TRACK_RAM_ADDR : unsigned(13 downto 0);
  signal tra : unsigned(15 downto 0);
  signal TRACK_RAM_DI : unsigned(7 downto 0);
  signal TRACK_RAM_WE : std_logic;

  signal CS_N, MOSI, MISO, SCLK : std_logic;

begin
   DIVIDER  <=CLK_28M;
   SRAM_CE  <= '0';
 	SRAM_CS1 <= '0';
	SRAM_CS0 <= '1';
	SRAM_ADSC <= '0';
	SRAM_ADSP <= '1';
	SRAM_ADV <= '1';
	SRAM_CLK <= CLK_14M;
	SRAM_GW <=   not ram_we; 
	SRAM_DQ(7 downto 0) <= D when ram_we = '1' else (others => 'Z');
   SRAM_ADDR(17) <= '0';
   SRAM_ADDR(16) <= '0';
   SRAM_UB_N <= '1'; --SRAM BWB
   SRAM_LB_N <= '1'; --SRAM BWA
   SRAM_WE_N <= '1'; --SRAM BWE
   SRAM_OE_N <= ram_we; 
	
	SPEAKER_OUT_L <= speaker;
	SPEAKER_OUT_R <= speaker;
	
  reset <= (not I_RESET) or power_on_reset;
  KBD_CLK <= CLK_14M;
  KBD_RST <= reset;
  power_on : process(CLK_14M)
  begin
    if rising_edge(CLK_14M) then
      if flash_clk(22) = '1' then
        power_on_reset <= '0';
      end if;
    end if;
  end process;

  -- In the Apple ][, this was a 555 timer
  flash_clkgen : process (CLK_14M)
  begin
    if rising_edge(CLK_14M) then
      flash_clk <= flash_clk + 1;
    end if;     
  end process;

  -- Paddle buttons
  GAMEPORT <=  "0000" & (not KEY(2 downto 0)) & "0";

  COLOR_LINE_CONTROL <= COLOR_LINE; --and SW(17);  -- Color or B&W mode
  
  core : entity work.apple2 port map (
    CLK_14M        => CLK_14M,
    CLK_2M         => CLK_2M,
    PRE_PHASE_ZERO => PRE_PHASE_ZERO,
    FLASH_CLK      => flash_clk(22),
    reset          => reset,
    ADDR           => ADDR,
    ram_addr       => SRAM_ADDR(15 downto 0),
    D              => D,
    ram_do         => SRAM_DQ(7 downto 0),
    PD             => PD,
    ram_we         => ram_we,
    VIDEO          => VIDEO,
    COLOR_LINE     => COLOR_LINE,
    HBL            => HBL,
    VBL            => VBL,
    LD194          => LD194,
    K              => KBD_DATA,
    read_key       => KBD_READ,
    AN             => open,
    GAMEPORT       => GAMEPORT,
    IO_SELECT      => IO_SELECT,
    DEVICE_SELECT  => DEVICE_SELECT,
    pcDebugOut     => cpu_pc,
    speaker        => speaker
    );

  vga : entity work.vga_controller port map (
    CLK_28M    => CLK_28M,
    VIDEO      => VIDEO,
    COLOR_LINE => COLOR_LINE_CONTROL,
    HBL        => HBL,
    VBL        => VBL,
    LD194      => LD194,
    VGA_CLK    => VGA_CLK,
    VGA_HS     => VGA_HS,
    VGA_VS     => VGA_VS,
    VGA_BLANK  => VGA_BLANK,
    VGA_R      => VGA_R,
    VGA_G      => VGA_G,
    VGA_B      => VGA_B
    );

  VGA_SYNC <= '0';

  disk : entity work.disk_ii port map (
    CLK_14M        => CLK_14M,
    CLK_2M         => CLK_2M,
    PRE_PHASE_ZERO => PRE_PHASE_ZERO,
    IO_SELECT      => IO_SELECT(6),
    DEVICE_SELECT  => DEVICE_SELECT(6),
    RESET          => reset,
    A              => ADDR,
    D_IN           => D,
    D_OUT          => PD,
    TRACK          => TRACK,
    TRACK_ADDR     => TRACK_ADDR,
    D1_ACTIVE      => D1_ACTIVE,
    D2_ACTIVE      => D2_ACTIVE,
    ram_write_addr => TRACK_RAM_ADDR,
    ram_di         => TRACK_RAM_DI,
    ram_we         => TRACK_RAM_WE
    );

  sdcard_interface : entity work.spi_controller port map (
    CLK_14M        => CLK_14M,
    reset          => reset,

    CS_N           => CS_N,
    MOSI           => MOSI,
    MISO           => MISO,
    SCLK           => SCLK,
    
    track          => TRACK,
    image          => image,
    
    ram_write_addr => TRACK_RAM_ADDR,
    ram_di         => TRACK_RAM_DI,
    ram_we         => TRACK_RAM_WE
    );

  image <= "0000000000";--SW(9 downto 0);

  CS_N_OUT <= CS_N;
  MOSI_OUT <= MOSI;
  MISO <= MISO_IN;
  SCLK_OUT <= SCLK;

  SRAM_DQ(15 downto 8) <= (others => 'Z');

end datapath;
