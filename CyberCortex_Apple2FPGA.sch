<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan3e" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_2" />
        <signal name="XLXN_3" />
        <signal name="clk" />
        <signal name="porte(3:0)" />
        <signal name="O_VSYNC" />
        <signal name="O_HSYNC" />
        <signal name="SRAM_DB(15:0)" />
        <signal name="SRAM_AB(17:0)" />
        <signal name="SRAM_OE" />
        <signal name="SRAM_CS0" />
        <signal name="SRAM_CS1" />
        <signal name="SRAM_ADSC" />
        <signal name="SRAM_ADSP" />
        <signal name="SRAM_ADV" />
        <signal name="SRAM_CLK" />
        <signal name="SRAM_GW" />
        <signal name="RESET" />
        <signal name="SRAM_CE" />
        <signal name="XLXN_4" />
        <signal name="XLXN_31(7:0)" />
        <signal name="XLXN_33" />
        <signal name="XLXN_34" />
        <signal name="XLXN_35" />
        <signal name="KEYBOARD_CLK" />
        <signal name="KEYBOARD_DATA" />
        <signal name="O_AUDIO_L" />
        <signal name="O_AUDIO_R" />
        <signal name="MISO" />
        <signal name="CS_N" />
        <signal name="MOSI" />
        <signal name="SCK" />
        <signal name="O_VIDEO_R(9:0)" />
        <signal name="O_VIDEO_G(9:0)" />
        <signal name="O_VIDEO_B(9:0)" />
        <port polarity="Input" name="clk" />
        <port polarity="Input" name="porte(3:0)" />
        <port polarity="Output" name="O_VSYNC" />
        <port polarity="Output" name="O_HSYNC" />
        <port polarity="BiDirectional" name="SRAM_DB(15:0)" />
        <port polarity="Output" name="SRAM_AB(17:0)" />
        <port polarity="Output" name="SRAM_OE" />
        <port polarity="Output" name="SRAM_CS0" />
        <port polarity="Output" name="SRAM_CS1" />
        <port polarity="Output" name="SRAM_ADSC" />
        <port polarity="Output" name="SRAM_ADSP" />
        <port polarity="Output" name="SRAM_ADV" />
        <port polarity="Output" name="SRAM_CLK" />
        <port polarity="Output" name="SRAM_GW" />
        <port polarity="Input" name="RESET" />
        <port polarity="Output" name="SRAM_CE" />
        <port polarity="Input" name="KEYBOARD_CLK" />
        <port polarity="Input" name="KEYBOARD_DATA" />
        <port polarity="Output" name="O_AUDIO_L" />
        <port polarity="Output" name="O_AUDIO_R" />
        <port polarity="Input" name="MISO" />
        <port polarity="Output" name="CS_N" />
        <port polarity="Output" name="MOSI" />
        <port polarity="Output" name="SCK" />
        <port polarity="Output" name="O_VIDEO_R(9:0)" />
        <port polarity="Output" name="O_VIDEO_G(9:0)" />
        <port polarity="Output" name="O_VIDEO_B(9:0)" />
        <blockdef name="APPLE2_TOP">
            <timestamp>2014-5-9T20:1:41</timestamp>
            <line x2="0" y1="16" y2="16" x1="64" />
            <line x2="496" y1="16" y2="16" x1="432" />
            <line x2="496" y1="80" y2="80" x1="432" />
            <line x2="496" y1="144" y2="144" x1="432" />
            <line x2="496" y1="-304" y2="-304" x1="432" />
            <line x2="496" y1="-240" y2="-240" x1="432" />
            <rect width="64" x="0" y="-572" height="24" />
            <line x2="0" y1="-560" y2="-560" x1="64" />
            <line x2="496" y1="-560" y2="-560" x1="432" />
            <line x2="496" y1="-496" y2="-496" x1="432" />
            <line x2="496" y1="-432" y2="-432" x1="432" />
            <line x2="496" y1="-624" y2="-624" x1="432" />
            <line x2="496" y1="-688" y2="-688" x1="432" />
            <line x2="0" y1="-752" y2="-752" x1="64" />
            <line x2="496" y1="-1200" y2="-1200" x1="432" />
            <line x2="496" y1="-1136" y2="-1136" x1="432" />
            <line x2="496" y1="-1072" y2="-1072" x1="432" />
            <line x2="496" y1="-1008" y2="-1008" x1="432" />
            <line x2="496" y1="-944" y2="-944" x1="432" />
            <line x2="496" y1="-880" y2="-880" x1="432" />
            <line x2="496" y1="-816" y2="-816" x1="432" />
            <line x2="436" y1="-1200" y2="-1200" x1="436" />
            <line x2="436" y1="-1136" y2="-1136" x1="436" />
            <line x2="436" y1="-1072" y2="-1072" x1="436" />
            <line x2="436" y1="-1008" y2="-1008" x1="436" />
            <line x2="436" y1="-944" y2="-944" x1="436" />
            <line x2="436" y1="-880" y2="-880" x1="436" />
            <line x2="436" y1="-816" y2="-816" x1="436" />
            <line x2="0" y1="-2528" y2="-2528" x1="64" />
            <line x2="0" y1="-2464" y2="-2464" x1="64" />
            <rect width="64" x="0" y="-2156" height="24" />
            <line x2="0" y1="-2144" y2="-2144" x1="64" />
            <line x2="496" y1="-2272" y2="-2272" x1="432" />
            <line x2="496" y1="-2208" y2="-2208" x1="432" />
            <line x2="496" y1="-2144" y2="-2144" x1="432" />
            <line x2="496" y1="-2080" y2="-2080" x1="432" />
            <line x2="496" y1="-2016" y2="-2016" x1="432" />
            <rect width="64" x="432" y="-1964" height="24" />
            <line x2="496" y1="-1952" y2="-1952" x1="432" />
            <rect width="64" x="432" y="-1900" height="24" />
            <line x2="496" y1="-1888" y2="-1888" x1="432" />
            <rect width="64" x="432" y="-1836" height="24" />
            <line x2="496" y1="-1824" y2="-1824" x1="432" />
            <rect width="64" x="432" y="-1340" height="24" />
            <line x2="496" y1="-1328" y2="-1328" x1="432" />
            <line x2="496" y1="-1648" y2="-1648" x1="432" />
            <line x2="496" y1="-1584" y2="-1584" x1="432" />
            <line x2="496" y1="-1520" y2="-1520" x1="432" />
            <line x2="496" y1="-1392" y2="-1392" x1="432" />
            <rect width="64" x="432" y="-1276" height="24" />
            <line x2="496" y1="-1264" y2="-1264" x1="432" />
            <rect width="368" x="64" y="-2576" height="3220" />
        </blockdef>
        <blockdef name="Divider">
            <timestamp>2011-6-21T3:21:58</timestamp>
            <rect width="256" x="64" y="-128" height="128" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="kb">
            <timestamp>2011-7-3T3:5:16</timestamp>
            <rect width="256" x="64" y="-320" height="320" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-300" height="24" />
            <line x2="384" y1="-288" y2="-288" x1="320" />
        </blockdef>
        <blockdef name="Clock">
            <timestamp>2015-1-20T7:33:31</timestamp>
            <line x2="464" y1="32" y2="32" x1="400" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="464" y1="-160" y2="-160" x1="400" />
            <line x2="464" y1="-96" y2="-96" x1="400" />
            <line x2="464" y1="-32" y2="-32" x1="400" />
            <rect width="336" x="64" y="-192" height="256" />
        </blockdef>
        <block symbolname="APPLE2_TOP" name="XLXI_1">
            <blockpin signalname="XLXN_3" name="CLK_28M" />
            <blockpin signalname="XLXN_2" name="CLK_14M" />
            <blockpin signalname="RESET" name="I_RESET" />
            <blockpin signalname="XLXN_31(7:0)" name="KBD_DATA(7:0)" />
            <blockpin signalname="porte(3:0)" name="KEY(3:0)" />
            <blockpin signalname="SRAM_DB(15:0)" name="SRAM_DQ(15:0)" />
            <blockpin signalname="XLXN_35" name="KBD_CLK" />
            <blockpin signalname="XLXN_33" name="KBD_RST" />
            <blockpin signalname="XLXN_34" name="KBD_READ" />
            <blockpin name="SRAM_UB_N" />
            <blockpin name="SRAM_LB_N" />
            <blockpin name="SRAM_WE_N" />
            <blockpin signalname="SRAM_CE" name="SRAM_CE" />
            <blockpin signalname="SRAM_CS0" name="SRAM_CS0" />
            <blockpin signalname="SRAM_CS1" name="SRAM_CS1" />
            <blockpin signalname="SRAM_ADSC" name="SRAM_ADSC" />
            <blockpin signalname="SRAM_ADSP" name="SRAM_ADSP" />
            <blockpin signalname="SRAM_ADV" name="SRAM_ADV" />
            <blockpin signalname="SRAM_CLK" name="SRAM_CLK" />
            <blockpin signalname="SRAM_GW" name="SRAM_GW" />
            <blockpin signalname="SRAM_OE" name="SRAM_OE_N" />
            <blockpin signalname="XLXN_4" name="DIVIDER" />
            <blockpin signalname="O_AUDIO_L" name="SPEAKER_OUT_L" />
            <blockpin signalname="O_AUDIO_R" name="SPEAKER_OUT_R" />
            <blockpin name="VGA_CLK" />
            <blockpin signalname="O_HSYNC" name="VGA_HS" />
            <blockpin signalname="O_VSYNC" name="VGA_VS" />
            <blockpin name="VGA_BLANK" />
            <blockpin name="VGA_SYNC" />
            <blockpin signalname="SRAM_AB(17:0)" name="SRAM_ADDR(17:0)" />
            <blockpin signalname="O_VIDEO_R(9:0)" name="VGA_R(9:0)" />
            <blockpin signalname="O_VIDEO_G(9:0)" name="VGA_G(9:0)" />
            <blockpin signalname="O_VIDEO_B(9:0)" name="VGA_B(9:0)" />
            <blockpin signalname="MISO" name="MISO_IN" />
            <blockpin signalname="CS_N" name="CS_N_OUT" />
            <blockpin signalname="MOSI" name="MOSI_OUT" />
            <blockpin signalname="SCK" name="SCLK_OUT" />
        </block>
        <block symbolname="Divider" name="XLXI_3">
            <blockpin signalname="XLXN_4" name="CLKIN_IN" />
            <blockpin signalname="XLXN_2" name="CLKDV_OUT" />
            <blockpin name="CLK0_OUT" />
        </block>
        <block symbolname="kb" name="XLXI_7">
            <blockpin signalname="KEYBOARD_CLK" name="PS2_Clk" />
            <blockpin signalname="KEYBOARD_DATA" name="PS2_Data" />
            <blockpin signalname="XLXN_35" name="CLK_14M" />
            <blockpin signalname="XLXN_34" name="read" />
            <blockpin signalname="XLXN_33" name="reset" />
            <blockpin signalname="XLXN_31(7:0)" name="K(7:0)" />
        </block>
        <block symbolname="Clock" name="XLXI_8">
            <blockpin signalname="clk" name="CLKIN_IN" />
            <blockpin signalname="XLXN_3" name="CLKFX_OUT" />
            <blockpin name="CLKFX180_OUT" />
            <blockpin name="CLKIN_IBUFG_OUT" />
            <blockpin name="CLK0_OUT" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="5440" height="7040">
        <instance x="2912" y="3664" name="XLXI_1" orien="R0">
        </instance>
        <instance x="2064" y="1296" name="XLXI_3" orien="R0">
        </instance>
        <branch name="XLXN_2">
            <wire x2="2912" y1="1200" y2="1200" x1="2448" />
        </branch>
        <branch name="XLXN_3">
            <wire x2="2624" y1="992" y2="992" x1="1760" />
            <wire x2="2624" y1="992" y2="1136" x1="2624" />
            <wire x2="2912" y1="1136" y2="1136" x1="2624" />
        </branch>
        <branch name="clk">
            <wire x2="1296" y1="992" y2="992" x1="1264" />
        </branch>
        <iomarker fontsize="28" x="1264" y="992" name="clk" orien="R180" />
        <branch name="porte(3:0)">
            <wire x2="2912" y1="1520" y2="1520" x1="2880" />
        </branch>
        <iomarker fontsize="28" x="2880" y="1520" name="porte(3:0)" orien="R180" />
        <branch name="O_VSYNC">
            <wire x2="3440" y1="1520" y2="1520" x1="3408" />
        </branch>
        <iomarker fontsize="28" x="3440" y="1520" name="O_VSYNC" orien="R0" />
        <branch name="O_HSYNC">
            <wire x2="3440" y1="1456" y2="1456" x1="3408" />
        </branch>
        <iomarker fontsize="28" x="3440" y="1456" name="O_HSYNC" orien="R0" />
        <branch name="SRAM_DB(15:0)">
            <wire x2="3440" y1="2400" y2="2400" x1="3408" />
        </branch>
        <iomarker fontsize="28" x="3440" y="2400" name="SRAM_DB(15:0)" orien="R0" />
        <branch name="SRAM_AB(17:0)">
            <wire x2="3440" y1="2336" y2="2336" x1="3408" />
        </branch>
        <iomarker fontsize="28" x="3440" y="2336" name="SRAM_AB(17:0)" orien="R0" />
        <branch name="SRAM_OE">
            <wire x2="3440" y1="2272" y2="2272" x1="3408" />
        </branch>
        <iomarker fontsize="28" x="3440" y="2272" name="SRAM_OE" orien="R0" />
        <branch name="SRAM_CS0">
            <wire x2="3440" y1="2464" y2="2464" x1="3408" />
        </branch>
        <iomarker fontsize="28" x="3440" y="2464" name="SRAM_CS0" orien="R0" />
        <branch name="SRAM_CS1">
            <wire x2="3440" y1="2528" y2="2528" x1="3408" />
        </branch>
        <iomarker fontsize="28" x="3440" y="2528" name="SRAM_CS1" orien="R0" />
        <branch name="SRAM_ADSC">
            <wire x2="3440" y1="2592" y2="2592" x1="3408" />
        </branch>
        <iomarker fontsize="28" x="3440" y="2592" name="SRAM_ADSC" orien="R0" />
        <branch name="SRAM_ADSP">
            <wire x2="3440" y1="2656" y2="2656" x1="3408" />
        </branch>
        <iomarker fontsize="28" x="3440" y="2656" name="SRAM_ADSP" orien="R0" />
        <branch name="SRAM_ADV">
            <wire x2="3440" y1="2720" y2="2720" x1="3408" />
        </branch>
        <iomarker fontsize="28" x="3440" y="2720" name="SRAM_ADV" orien="R0" />
        <branch name="SRAM_CLK">
            <wire x2="3440" y1="2784" y2="2784" x1="3408" />
        </branch>
        <iomarker fontsize="28" x="3440" y="2784" name="SRAM_CLK" orien="R0" />
        <branch name="SRAM_GW">
            <wire x2="3440" y1="2848" y2="2848" x1="3408" />
        </branch>
        <iomarker fontsize="28" x="3440" y="2848" name="SRAM_GW" orien="R0" />
        <branch name="RESET">
            <wire x2="2912" y1="2912" y2="2912" x1="2880" />
        </branch>
        <iomarker fontsize="28" x="2880" y="2912" name="RESET" orien="R180" />
        <branch name="SRAM_CE">
            <wire x2="3440" y1="2976" y2="2976" x1="3408" />
        </branch>
        <iomarker fontsize="28" x="3440" y="2976" name="SRAM_CE" orien="R0" />
        <branch name="XLXN_4">
            <wire x2="1984" y1="1072" y2="1200" x1="1984" />
            <wire x2="2064" y1="1200" y2="1200" x1="1984" />
            <wire x2="3760" y1="1072" y2="1072" x1="1984" />
            <wire x2="3760" y1="1072" y2="3040" x1="3760" />
            <wire x2="3760" y1="3040" y2="3040" x1="3408" />
        </branch>
        <instance x="2944" y="4816" name="XLXI_7" orien="R0">
        </instance>
        <branch name="XLXN_31(7:0)">
            <wire x2="2320" y1="3104" y2="5184" x1="2320" />
            <wire x2="3744" y1="5184" y2="5184" x1="2320" />
            <wire x2="2912" y1="3104" y2="3104" x1="2320" />
            <wire x2="3744" y1="4528" y2="4528" x1="3328" />
            <wire x2="3744" y1="4528" y2="5184" x1="3744" />
        </branch>
        <branch name="XLXN_33">
            <wire x2="2512" y1="4432" y2="4784" x1="2512" />
            <wire x2="2944" y1="4784" y2="4784" x1="2512" />
            <wire x2="3472" y1="4432" y2="4432" x1="2512" />
            <wire x2="3472" y1="3168" y2="3168" x1="3408" />
            <wire x2="3472" y1="3168" y2="4432" x1="3472" />
        </branch>
        <branch name="XLXN_34">
            <wire x2="2544" y1="4448" y2="4720" x1="2544" />
            <wire x2="2944" y1="4720" y2="4720" x1="2544" />
            <wire x2="3456" y1="4448" y2="4448" x1="2544" />
            <wire x2="3456" y1="3232" y2="3232" x1="3408" />
            <wire x2="3456" y1="3232" y2="4448" x1="3456" />
        </branch>
        <branch name="XLXN_35">
            <wire x2="2592" y1="4464" y2="4656" x1="2592" />
            <wire x2="2944" y1="4656" y2="4656" x1="2592" />
            <wire x2="3488" y1="4464" y2="4464" x1="2592" />
            <wire x2="3488" y1="3104" y2="3104" x1="3408" />
            <wire x2="3488" y1="3104" y2="4464" x1="3488" />
        </branch>
        <branch name="KEYBOARD_CLK">
            <wire x2="2944" y1="4528" y2="4528" x1="2912" />
        </branch>
        <iomarker fontsize="28" x="2912" y="4528" name="KEYBOARD_CLK" orien="R180" />
        <branch name="KEYBOARD_DATA">
            <wire x2="2944" y1="4592" y2="4592" x1="2912" />
        </branch>
        <iomarker fontsize="28" x="2912" y="4592" name="KEYBOARD_DATA" orien="R180" />
        <branch name="O_AUDIO_L">
            <wire x2="3424" y1="3360" y2="3360" x1="3408" />
            <wire x2="3680" y1="3360" y2="3360" x1="3424" />
        </branch>
        <branch name="O_AUDIO_R">
            <wire x2="3424" y1="3424" y2="3424" x1="3408" />
            <wire x2="3680" y1="3424" y2="3424" x1="3424" />
        </branch>
        <iomarker fontsize="28" x="3680" y="3360" name="O_AUDIO_L" orien="R0" />
        <iomarker fontsize="28" x="3680" y="3424" name="O_AUDIO_R" orien="R0" />
        <branch name="MISO">
            <wire x2="2912" y1="3680" y2="3680" x1="2880" />
        </branch>
        <iomarker fontsize="28" x="2880" y="3680" name="MISO" orien="R180" />
        <branch name="CS_N">
            <wire x2="3424" y1="3680" y2="3680" x1="3408" />
            <wire x2="3600" y1="3680" y2="3680" x1="3424" />
        </branch>
        <branch name="MOSI">
            <wire x2="3424" y1="3744" y2="3744" x1="3408" />
            <wire x2="3600" y1="3744" y2="3744" x1="3424" />
        </branch>
        <branch name="SCK">
            <wire x2="3424" y1="3808" y2="3808" x1="3408" />
            <wire x2="3600" y1="3808" y2="3808" x1="3424" />
        </branch>
        <iomarker fontsize="28" x="3600" y="3680" name="CS_N" orien="R0" />
        <iomarker fontsize="28" x="3600" y="3744" name="MOSI" orien="R0" />
        <iomarker fontsize="28" x="3600" y="3808" name="SCK" orien="R0" />
        <branch name="O_VIDEO_R(9:0)">
            <wire x2="3424" y1="1712" y2="1712" x1="3408" />
            <wire x2="3872" y1="1712" y2="1712" x1="3424" />
        </branch>
        <branch name="O_VIDEO_G(9:0)">
            <wire x2="3424" y1="1776" y2="1776" x1="3408" />
            <wire x2="3872" y1="1776" y2="1776" x1="3424" />
        </branch>
        <branch name="O_VIDEO_B(9:0)">
            <wire x2="3424" y1="1840" y2="1840" x1="3408" />
            <wire x2="3872" y1="1840" y2="1840" x1="3424" />
        </branch>
        <iomarker fontsize="28" x="3872" y="1840" name="O_VIDEO_B(9:0)" orien="R0" />
        <iomarker fontsize="28" x="3872" y="1776" name="O_VIDEO_G(9:0)" orien="R0" />
        <iomarker fontsize="28" x="3872" y="1712" name="O_VIDEO_R(9:0)" orien="R0" />
        <instance x="1296" y="1152" name="XLXI_8" orien="R0">
        </instance>
    </sheet>
</drawing>