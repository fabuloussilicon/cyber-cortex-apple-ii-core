library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity disk_ii_rom is
 port (
   addr : in  unsigned(7 downto 0);
   clk  : in  std_logic;
   dout : out unsigned(7 downto 0));
end disk_ii_rom;

architecture rtl of disk_ii_rom is
  type rom_array is array(0 to 255) of unsigned(7 downto 0);

  constant ROM : rom_array := (
     X"00", X"11", X"22", X"33", X"44", X"55", X"66", X"77",
     X"88", X"99", X"aa", X"bb", X"cc", X"dd", X"ee", X"ff",
	  X"00", X"11", X"22", X"33", X"44", X"55", X"66", X"77",
     X"88", X"99", X"aa", X"bb", X"cc", X"dd", X"ee", X"ff",
	  X"00", X"11", X"22", X"33", X"44", X"55", X"66", X"77",
     X"88", X"99", X"aa", X"bb", X"cc", X"dd", X"ee", X"ff",
	  X"00", X"11", X"22", X"33", X"44", X"55", X"66", X"77",
     X"88", X"99", X"aa", X"bb", X"cc", X"dd", X"ee", X"ff",
	  X"00", X"11", X"22", X"33", X"44", X"55", X"66", X"77",
     X"88", X"99", X"aa", X"bb", X"cc", X"dd", X"ee", X"ff",
	  X"00", X"11", X"22", X"33", X"44", X"55", X"66", X"77",
     X"88", X"99", X"aa", X"bb", X"cc", X"dd", X"ee", X"ff",
	  X"00", X"11", X"22", X"33", X"44", X"55", X"66", X"77",
     X"88", X"99", X"aa", X"bb", X"cc", X"dd", X"ee", X"ff",
	  X"00", X"11", X"22", X"33", X"44", X"55", X"66", X"77",
     X"88", X"99", X"aa", X"bb", X"cc", X"dd", X"ee", X"ff",
	  X"00", X"11", X"22", X"33", X"44", X"55", X"66", X"77",
     X"88", X"99", X"aa", X"bb", X"cc", X"dd", X"ee", X"ff",
	  X"00", X"11", X"22", X"33", X"44", X"55", X"66", X"77",
     X"88", X"99", X"aa", X"bb", X"cc", X"dd", X"ee", X"ff",
	  X"00", X"11", X"22", X"33", X"44", X"55", X"66", X"77",
     X"88", X"99", X"a2", X"bb", X"cc", X"dd", X"ee", X"ff",
	  X"00", X"11", X"22", X"33", X"44", X"55", X"66", X"77",
     X"88", X"99", X"aa", X"bb", X"cc", X"dd", X"ee", X"ff",
	  X"00", X"11", X"22", X"33", X"44", X"55", X"66", X"77",
     X"88", X"99", X"aa", X"bb", X"cc", X"dd", X"ee", X"ff",
	  X"00", X"11", X"22", X"33", X"44", X"55", X"66", X"77",
     X"88", X"99", X"aa", X"bb", X"cc", X"dd", X"ee", X"ff",
	  X"00", X"11", X"22", X"33", X"44", X"55", X"66", X"77",
     X"88", X"99", X"aa", X"bb", X"cc", X"dd", X"fe", X"ff",
	  X"00", X"11", X"22", X"33", X"44", X"55", X"66", X"77",
     X"88", X"99", X"aa", X"bb", X"cc", X"dd", X"ee", X"ff");

begin

process (clk)
  begin
    if rising_edge(clk) then
      dout <= ROM(TO_INTEGER(addr));
    end if;
  end process;

end rtl;